# Python Excel数据处理自动化生成Word文档（含索引目录 分页）

#### 介绍
通过Python 批量处理遍历文件目录中的Excel文件，对Excel数据进行筛选、排序、合并等处理并获取到结果数据，存储到DataFrame中；将DataFrame中的数据通过Python docx 写入到提前读取的Word模板中，自动化生成Word字典文件（含索引目录及分页）
流程：遍历目录->获取Excel list->数据筛选、合并->读取Word模板->数据写入Word（生成文档首页、目录标题、正文、表格、样式处理等）->更新索引目录->生成结果Word文件

#### Excel源数据文档样例
![输入图片说明](https://images.gitee.com/uploads/images/2021/0122/102808_fc3e1aa4_117980.png "屏幕截图.png")

#### Word模板文档样例
![输入图片说明](https://images.gitee.com/uploads/images/2021/0122/103121_22e87a34_117980.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0122/103143_28a5b618_117980.png "屏幕截图.png")

#### Word结果文档样例
![输入图片说明](https://images.gitee.com/uploads/images/2021/0122/102919_4a7c6798_117980.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0122/102941_cc8d12a9_117980.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0122/103016_e9a3db38_117980.png "屏幕截图.png")